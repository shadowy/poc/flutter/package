import 'package:flutter/material.dart';

class ApplicationColors {
  ApplicationColors._();

  static const Color logo = Color(_primaryValue);
  static const Color text = Color(0xffffffff);

  static const MaterialColor primary = MaterialColor(_primaryValue, <int, Color>{
    50: Color(0xFFE7ECF3),
    100: Color(0xFFC3D0E1),
    200: Color(0xFF9BB1CD),
    300: Color(0xFF7392B9),
    400: Color(0xFF557AAA),
    500: Color(_primaryValue),
    600: Color(0xFF315B93),
    700: Color(0xFF2A5189),
    800: Color(0xFF23477F),
    900: Color(0xFF16356D),
  });
  static const int _primaryValue = 0xFF37639B;

  static const MaterialColor accentM = MaterialColor(_accentValue, <int, Color>{
    100: Color(0xFFA6C3FF),
    200: Color(_accentValue),
    400: Color(0xFF407EFF),
    700: Color(0xFF266DFF),
  });
  static const int _accentValue = 0xFF73A0FF;
}
