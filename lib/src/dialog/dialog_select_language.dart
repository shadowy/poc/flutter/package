import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';

typedef SetState = void Function(VoidCallback fn);

void showDialogSelectLanguage(BuildContext context) {
  Locale appLocale = Localizations.localeOf(context);
  String lang = appLocale.languageCode;

  void setLang(String? value, setState) {
    setState(() {
      lang = value!;
      GetIt.I.get<LanguageService>().setLocale(value);
    });
  }

  showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) =>
          StatefulBuilder(
              builder: (context, setState) =>
                  AlertDialog(
                    title: Text(
                        PackageLocalizations.of(context)!
                            .dialogLanguageSelectTitle),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ListTile(
                          title: Text(
                              PackageLocalizations.of(context)!.txtEnglish),
                          leading: Radio<String>(
                            value: 'en',
                            groupValue: lang,
                            onChanged: (value) => setLang(value, setState),),
                          onTap: () => setLang('en', setState),
                        ),
                        ListTile(
                          title: Text(
                              PackageLocalizations.of(context)!.txtItaliano),
                          leading: Radio<String>(
                            value: 'it',
                            groupValue: lang,
                            onChanged: (value) => setLang(value, setState),),
                          onTap: () => setLang('it', setState),
                        ),
                        ListTile(
                          title: Text(
                              PackageLocalizations.of(context)!.txtUkrainian),
                          leading: Radio<String>(
                            value: 'uk',
                            groupValue: lang,
                            onChanged: (value) => setLang(value, setState),),
                          onTap: () => setLang('uk', setState),
                        ),
                        ListTile(
                          title: Text(
                              PackageLocalizations.of(context)!.txtRussian),
                          leading: Radio<String>(
                            value: 'ru',
                            groupValue: lang,
                            onChanged: (value) => setLang(value, setState),),
                          onTap: () => setLang('ru', setState),
                        )
                      ],
                    ),
                    actions: [
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'cancel'),
                        child: Text(
                            PackageLocalizations.of(context)!
                                .commonButtonCancel),
                      )
                    ],
                  )
          ));
}