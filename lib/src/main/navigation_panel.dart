import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';

class NavigationPanel extends StatefulWidget {
  final List<Widget> children;

  const NavigationPanel({Key? key,required this.children}) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  _NavigationPanelState createState() => _NavigationPanelState(children);
}

class _NavigationPanelState extends State<NavigationPanel> {
  late StreamSubscription _userInformationListener;
  String userName = '';
  final List<Widget> children;

  _NavigationPanelState(this.children);

  @override
  void initState() {
    super.initState();
    _userInformationListener = GetIt.I.get<UserService>().userInformation.listen(((event) {
      setState(() {
        userName = event == null ? '' : event.name;
      });
    }));
  }

  @override
  void dispose() {
    super.dispose();
    _userInformationListener.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            padding: const EdgeInsets.all( 3 * space),
            decoration: BoxDecoration(
              color:  ApplicationColors.primary.shade400, //Theme.of(context).colorScheme.tertiary
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Image(
                    image: AssetImage('assets/logo_avtologistika_rsz.png', package: 'package'),
                    fit: BoxFit.scaleDown
                ),
                Container(height: 2 * space,),
                Text(userName, style: const TextStyle(color: ApplicationColors.text, fontWeight: FontWeight.w900)),
                Container(height: space,),
                Text(PackageLocalizations.of(context)!.navigationPanelHeaderTitle,
                    style: const TextStyle(color: ApplicationColors.text)
                ),
              ]
            ),
          ),
          ...children,
        ],
      ),
    );
  }
}