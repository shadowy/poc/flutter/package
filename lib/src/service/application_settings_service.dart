import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:package_info_plus/package_info_plus.dart';

typedef CreatePage = Widget Function();

class ApplicationSettingsService {
  final _storage = const FlutterSecureStorage();
  final Map<String, String> _default = <String, String>{};
  Map<String, String> _all = <String, String>{};
  String _version = 'debug';
  CreatePage? _mainPage;

  String get version => _version;

  setDefaultValue(String key, String value) {
    _default[key] = value;
  }

  setMainPage(CreatePage page) {
    _mainPage = page;
  }

  Widget getMainPage() {
    return _mainPage!();
  }

  Future<void> init() async {
    _all = (await _storage.readAll())
        .map((key, value) => MapEntry<String, String>(key, value == '' ? _default[key] ?? '' : value));
    _default.forEach((key, value) {
      if (_all.containsKey(key)) {
        return;
      }
      _all[key] = value;
    });
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _version = packageInfo.version;
  }

  String get(String key) {
    return _all[key] ?? '';
  }

  Future<void> set(String key, String value) async {
    if (value == '') {
      await _storage.delete(key: key);
    } else {
      await _storage.write(key: key, value: value);
    }
    _all[key] = value;
  }
}
