import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';

class LanguageService {
  final _locale = StreamController<Locale>();

  get locale => _locale.stream;

  void setLocale(String locale) {
    _locale.add(Locale(locale));
    GetIt.I.get<ApplicationSettingsService>().set('lang', locale);
  }
}
