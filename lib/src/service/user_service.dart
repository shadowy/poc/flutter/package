import 'dart:async';

import 'package:package/src/service/model/user-information.dart';
import 'package:rxdart/rxdart.dart';

class UserService {
  final _userInformation = BehaviorSubject<UserInformation?>();

  Stream<UserInformation?> get userInformation => _userInformation.stream;

  UserService() {
    _userInformation.add(null);
  }

  login() {
    _userInformation.add(UserInformation());
  }

  logout() {
    _userInformation.add(null);
  }
}
