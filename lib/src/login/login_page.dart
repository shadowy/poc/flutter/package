import 'package:flutter/material.dart';
import '../application_colors.dart';
import '../settings.dart';
import 'view/login_form.dart';
import 'view/login_footer.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
              child: Container(
            child: const Image(
              image: AssetImage('assets/logo_black.png', package: 'package'),
              fit: BoxFit.scaleDown,
            ),
            color: ApplicationColors.logo,
          )),
          Expanded(
              child: Column(
            children: [
              const LoginForm(),
              Container(height: 2 * space),
            ],
          )),
          const LoginFooter()
        ],
      ),
    ));
  }
}
