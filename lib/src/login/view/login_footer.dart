import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';
import '../../dialog/dialog_select_language.dart';

class LoginFooter extends StatefulWidget {
  const LoginFooter({Key? key}) : super(key: key);

  @override
  _LoginFooterState createState() => _LoginFooterState();
}

class _LoginFooterState extends State<LoginFooter> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Center(
        child: Text(GetIt.I.get<ApplicationSettingsService>().version),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Row(children: [
              ElevatedButton(
                child: const Icon(Icons.language),
                onPressed: () => showDialogSelectLanguage(context),
              ),
              Container(
                width: space,
              ),
              ElevatedButton(
                child: const Icon(Icons.help),
                onPressed: () => {},
              ),
              Container(
                width: space,
              ),
              ElevatedButton(
                child: const Icon(Icons.vpn_key),
                onPressed: () => {},
              ),
              Container(
                width: space,
              ),
              ElevatedButton(
                child: const Icon(Icons.settings),
                onPressed: () => {},
              ),
              Container(
                width: space,
              ),
              ElevatedButton(
                child: const Icon(Icons.lock),
                onPressed: () => {},
              ),
            ]),
          )
        ],
      )
    ]);
  }
}
