import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:package/package.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width - 2 * 4 * space,
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(labelText: PackageLocalizations.of(context)!.loginFormUsername),
            ),
            TextField(
              decoration: InputDecoration(labelText: PackageLocalizations.of(context)!.loginFormPassword),
            ),
            Container(height: 2 * space),
            SizedBox(
              width: MediaQuery.of(context).size.width - 2 * space,
              child: ElevatedButton(
                child: Text(PackageLocalizations.of(context)!.loginFormBtnLogin),
                onPressed: () {
                  GetIt.I.get<UserService>().login();
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) => GetIt.I.get<ApplicationSettingsService>().getMainPage()));
                },
              ),
            ),
          ],
        ));
  }
}
